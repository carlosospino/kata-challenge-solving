# Kata Challenge Solving



## Context

This repository contains the solutions for the Kata's Challenges.

## References
- [ ] [Kata 1](https://www.codewars.com/kata/5168bb5dfe9a00b126000018/train/java)
- [ ] [Kata 2](https://www.codewars.com/kata/53417de006654f4171000587/train/java)



## Tools and technologies

<div style="display:flex;flex-flow:row;align-items:center;gap:20px">
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/gradle/gradle-plain-wordmark.svg" alt="Gradle" style="height: 100px; width:100px;"/>
<img src="https://camo.githubusercontent.com/abbaedce4b226ea68b0fd43521472b0b146d5ed57956116f69752f43e7ddd7d8/68747470733a2f2f6a756e69742e6f72672f6a756e6974352f6173736574732f696d672f6a756e6974352d6c6f676f2e706e67" alt="Junit5" style="height: 50px; width:50px;"/>
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original-wordmark.svg" alt="Java" style="height: 100px; width:100px;"/>
</div>

## Author
Carlos Ospino