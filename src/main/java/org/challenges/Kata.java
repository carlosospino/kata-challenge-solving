package org.challenges;

public class Kata {

    public static String solution(String str){
        //Convert word to array
        String[] word = str.split("");
        String[] reversedWord = new String[word.length];

        //Check the letters of the word and reverse their position.
        for (int letterPos = 0; letterPos < word.length; letterPos++){
            reversedWord[(word.length - 1) - letterPos] = word[letterPos];
        }

        //Return reversed Array as concatenated String
        return  String.join("", reversedWord);
    }

}