package org.challenges;

public class Game {
    private int playerScoreOne = 0;
    private int playerScoreTwo = 0;
    private char[] cardRanks = {'2','3','4','5','6','7','8','9','T','J','Q','K','A'};
    public  String winner(String[] deckSteve, String[] deckJosh){
        playerScoreOne = 0;
        playerScoreTwo = 0;
        final int DECK_LENGTH = deckSteve.length;

        //Check every card
        for (int currentCardPos = 0; currentCardPos < DECK_LENGTH; currentCardPos++){
            int playerOneCardRank = 0;
            int playerTwoCardRank = 0;

            char currentPlayerOneValue = deckSteve[currentCardPos].toCharArray()[0];
            char currentPlayerTwoValue = deckJosh[currentCardPos].toCharArray()[0];

            //Check rank of cards.
            for (int currentRankPos = 0; currentRankPos < cardRanks.length; currentRankPos++){
                if (cardRanks[currentRankPos] == currentPlayerOneValue){
                    playerOneCardRank = currentRankPos;
                }
                if (cardRanks[currentRankPos] == currentPlayerTwoValue){
                    playerTwoCardRank = currentRankPos;
                }
            }

            //Evaluate rank of cards.
            if (playerOneCardRank > playerTwoCardRank){
                playerScoreOne += 1;
            } else if (playerOneCardRank < playerTwoCardRank) {
                playerScoreTwo += 1;
            }
        }

        //Returns the result of game.
        if (playerScoreOne > playerScoreTwo){
            return String.format("Steve wins %s to %s", playerScoreOne, playerScoreTwo);
        }else if (playerScoreOne < playerScoreTwo){
            return String.format("Josh wins %s to %s", playerScoreTwo, playerScoreOne);
        }
        else {
            return "Tie";
        }
    }
}
