package org.challenges;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private Game game = new Game();

    @Test
    public void testGame() {
        assertEquals(
                "Steve wins 2 to 1",
                game.winner(new String[]{"A", "7", "8"}, new String[]{"K", "5", "9"})
        );

        assertEquals(
                "Tie",
                game.winner(new String[]{"T"}, new String[]{"T"})
        );
    }
}